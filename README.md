### KATJA

## Manual build
```
cd katja
docker-compose up
```

## Prebuild images
```
cd katja
docker-compose -f docker-compose-prebuild.yml up
```

## Access
When KATJA is up open in browser with:
http://localhost

Login with
Username:Admin
password: katjasecret
