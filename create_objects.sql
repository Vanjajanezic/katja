-- Table: public.objects

-- DROP TABLE public.objects;

CREATE TABLE public.objects
(
    pagename character varying(250) COLLATE pg_catalog."default" NOT NULL,
    geom geometry(Point,4326) NOT NULL,
    natcatnumb character varying(24) COLLATE pg_catalog."default" NOT NULL,
    clubcatnumb character varying(24) COLLATE pg_catalog."default",
    code character varying(24) COLLATE pg_catalog."default" NOT NULL,
    name character varying(128) COLLATE pg_catalog."default",
    nattype1 character varying(128) COLLATE pg_catalog."default",
    nattype2 character varying(128) COLLATE pg_catalog."default",
    mapsymbol integer,
    nwgs84 double precision,
    ewgs84 double precision,
    elevation integer,
    locatedby integer,
    accuracy integer,
    geogunit1 character varying(128) COLLATE pg_catalog."default",
    geogunit2 character varying(128) COLLATE pg_catalog."default",
    geogunit3 character varying(128) COLLATE pg_catalog."default",
    length real,
    depth real,
    measured integer,
    discoverydate date,
    organization character varying(250) COLLATE pg_catalog."default",
    author character varying(250) COLLATE pg_catalog."default",
    entrycomment text COLLATE pg_catalog."default",
    condition text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    taccess integer,
    lasteditor character varying(64) COLLATE pg_catalog."default",
    entryhistory text COLLATE pg_catalog."default",
    "timestamp" timestamp without time zone,
    natregstatus integer,
    image character varying(128) COLLATE pg_catalog."default",
    locationcomment character varying COLLATE pg_catalog."default",
    surface real,
    volume real,
    sync_level integer,
    CONSTRAINT objects_pkey PRIMARY KEY (pagename)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.objects
    OWNER to postgres;
COMMENT ON TABLE public.objects
    IS 'This is main table for storing objects data outside katja wiki tables. It is used for exports, for map entry, for mapping , statistics, etc ... Table link to several other tables, which provide string values for numerical fields in chosen language.';

COMMENT ON COLUMN public.objects.pagename
    IS 'Thi si primary key. It holds full pagename of the object description page in wiki. Usually consists of name + code  + natcatnumb';

COMMENT ON COLUMN public.objects.geom
    IS 'Geometry holds  POSTGIS point geometry in WGS84';

COMMENT ON COLUMN public.objects.natcatnumb
    IS 'National catalog number or code. Can hold alphanumeric code.';

COMMENT ON COLUMN public.objects.clubcatnumb
    IS 'Hold club catalog number or code if available';

COMMENT ON COLUMN public.objects.code
    IS 'Hold KATJA code. Usually consist of random number or alphanumeric code. Is part of pagename and guaranties uniqueness of pagename.';

COMMENT ON COLUMN public.objects.nattype1
    IS 'National typology code. This is also name of wiki category';

COMMENT ON COLUMN public.objects.nattype2
    IS 'Hold additional national typology code in case nation uses several typology systems';

COMMENT ON COLUMN public.objects.mapsymbol
    IS 'Coded number of map symbol to be used to present objcet on maps. It is in its own case a sort of typology system.';

COMMENT ON COLUMN public.objects.nwgs84
    IS 'Hold latitude of WGS84 coorduinate part. Obsolete in final version.';

COMMENT ON COLUMN public.objects.ewgs84
    IS 'Longitude part of WGS84 coordinate. Oboslete in final version.';

COMMENT ON COLUMN public.objects.elevation
    IS 'Elevation above sea level in meters of object entrance.';

COMMENT ON COLUMN public.objects.locatedby
    IS 'Holds number that describes method of recording object entrance location.';

COMMENT ON COLUMN public.objects.accuracy
    IS 'Location accuracy in meters. If data not available, enter 0';

COMMENT ON COLUMN public.objects.geogunit1
    IS 'Geographic unit or subunit in which the object location is. Is normally wiki category.';

COMMENT ON COLUMN public.objects.geogunit2
    IS 'Geographic unit or subunit in which the object location is. Used for alternate geographic division.';

COMMENT ON COLUMN public.objects.geogunit3
    IS 'Geographic unit or subunit in which the object location is. Used for alternate geographic division.';

COMMENT ON COLUMN public.objects.length
    IS 'Complete length of measured polygon.';

COMMENT ON COLUMN public.objects.depth
    IS 'Relative depth or heigh of the object regarding entrance vs deepest point.';

COMMENT ON COLUMN public.objects.discoverydate
    IS 'Holds date of discovery in YYYY-MM-DD format. Does not hold date of entry. But actual date of first documented visit to the object.';

COMMENT ON COLUMN public.objects.organization
    IS 'Hold name of organization resonsible for first discovery. Usually this is name of the caving club that made first discovery.';

COMMENT ON COLUMN public.objects."timestamp"
    IS 'Time stamp of last data editing of this row';

COMMENT ON COLUMN public.objects.image
    IS 'Holds pagename of the image to be used in infobox';

COMMENT ON COLUMN public.objects.locationcomment
    IS 'Comment to location, coordinates and accuracy';

COMMENT ON COLUMN public.objects.surface
    IS 'Hold estimated surface in sqr. meters.';

COMMENT ON COLUMN public.objects.volume
    IS 'Hold estimated volume of object in cub. meters.';

COMMENT ON COLUMN public.objects.sync_level
    IS 'Defines if this record should be synced to KATJA global tables and on what level';

-- Index: idx_clubcatnumb

-- DROP INDEX public.idx_clubcatnumb;

CREATE INDEX idx_clubcatnumb
    ON public.objects USING btree
    (clubcatnumb COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: idx_code

-- DROP INDEX public.idx_code;

CREATE INDEX idx_code
    ON public.objects USING btree
    (code COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: idx_name

-- DROP INDEX public.idx_name;

CREATE INDEX idx_name
    ON public.objects USING btree
    (name COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: idx_natcatnumb

-- DROP INDEX public.idx_natcatnumb;

CREATE INDEX idx_natcatnumb
    ON public.objects USING btree
    (natcatnumb COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: sidx_objects_geom

-- DROP INDEX public.sidx_objects_geom;

CREATE INDEX sidx_objects_geom
    ON public.objects USING gist
    (geom)
    TABLESPACE pg_default;
