-- Table: public.taccess

-- DROP TABLE public.taccess;

CREATE TABLE public.taccess
(
    id integer NOT NULL,
    english character varying(128) COLLATE pg_catalog."default",
    slovene character varying(128) COLLATE pg_catalog."default",
    bulgarian character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT access_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.taccess
    OWNER to postgres;
COMMENT ON TABLE public.taccess
    IS 'Map value table for access field in objects table';

-- Table: public.locatedby

-- DROP TABLE public.locatedby;

CREATE TABLE public.locatedby
(
    id integer NOT NULL,
    english character varying(128) COLLATE pg_catalog."default",
    slovene character varying(128) COLLATE pg_catalog."default",
    bulgarian character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT locatedby_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.locatedby
    OWNER to postgres;
COMMENT ON TABLE public.locatedby
    IS 'Value map table for locatedby field in objects table';

COMMENT ON COLUMN public.locatedby.id
    IS 'ID  links to locatedby field on objects table.';

COMMENT ON COLUMN public.locatedby.english
    IS 'English description of value';

COMMENT ON COLUMN public.locatedby.slovene
    IS 'Slovenski opis vrednosti';

-- Table: public.natregstatus

-- DROP TABLE public.natregstatus;

CREATE TABLE public.natregstatus
(
    id integer NOT NULL,
    english character varying(128) COLLATE pg_catalog."default",
    slovene character varying(128) COLLATE pg_catalog."default",
    bulgarian character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT natregstatus_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.natregstatus
    OWNER to postgres;
COMMENT ON TABLE public.natregstatus
    IS 'Value map table for natregstatus field in table objects. Hold national registry status.';

COMMENT ON COLUMN public.natregstatus.id
    IS 'ID field links to natregstatus in objects table';

COMMENT ON COLUMN public.natregstatus.english
    IS 'English description';

COMMENT ON COLUMN public.natregstatus.slovene
    IS 'Slovenski opis';


