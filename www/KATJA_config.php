<?php
## this is KATJA
## input forms configuration file

## database settings
$host='katjadb';
$db = 'katja';
$dbport='5432';
$username = 'wikiuser';
$password = 'katjasecret';

## KATJA reader
## username and password of 
## reg. user that has reda only rights
## used by php scripts that aquire data
## from wiki api
$katja_reader="Admin";
$reader_password="geslojeskrito";
$NationalTypology1Name="National typology 1"; #sets name for national typolog 1 category
$NationalTypology2Name="National typology 2"; #sets name for national typolog 2 category

## wiki host
## we pull this form environment
$wikihost=$_ENV["WIKIHOST"]; 

## set host and maps for forms map
$maphost='name geoserver';

## starting coordinates and zoom, these will be used
## when form opens, form map, marker position and zoom will be used
$init_latitude='45.7483603';
$init_longitude='15.0238037';
$init_zoom='12';


## Here we set elevation parameters
## if enabled.  sytem will try to determine elevation from
## map layer defined
$auto_elevation=false; //set this to true to enable automatic elevation functions
$elevation_layer='set here name of layer';

## Allowed position difference in meters
## If any of two objects are closer then this value
## error will be invoked upon data entry, and new data will not be saved
## use some sensible value between 3m and 15m
$AllowedPositionDiff='300';


## geographic units
##
$t_geogunit1='t_geogunit1';  #name of geographic unit 1 table
$t_geogunit1_name ='name';   # column name to look in
$t_geogunit1_crs='4326';     # CRS of geographic unit 1 table

$t_geogunit2='t_geogunit2';  #name of geographic unit 2 table
$t_geogunit2_name ='name';    # column name to look in
$t_geogunit2_crs='4326';     # CRS of geographic unit 2 table

$t_geogunit3='t_geogunit3';  #name of geographic unit 3 table
$t_geogunit3_name ='name';    # column name to look in
$t_geogunit3_crs='4326';     # CRS of geographic unit 3 table

?>
