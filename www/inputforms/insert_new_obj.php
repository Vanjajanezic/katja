<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
</head>
</body>
<?php
### Save new object record to database 
// debug section, comment out
error_reporting(E_ALL);
ini_set('display_errors', 1);
// end of debug




// ****************************** IMPORTANT *****************************
// ************    CHECK FOR EMPTY FIELDS
// sync fields etc
// if natcatnumb 0000 DO NOT CHECK
// // set most of defaults in form
// check for combination natcatnumb 0000 and registered status (not allowed)
// ****************************** end of IMPORTANT *****************************


//check username
if (empty($_POST['lasteditor'])){
echo 'ERROR. There is no username';die;
}

//check object name
if (empty($_POST['name'])){
echo 'Error! There si NO object NAME. At least set name of the object!<br>';
echo 'Click <b>back</b> in your browser, set at least coordinates and object name and try again';die;
}

require_once '../KATJA_config.php'; //read config file
$dsn = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password"; //set connection 
try{
 // create a PostgreSQL database connection
$conn = new PDO($dsn);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 // display a message if connected to the PostgreSQL successfully
 if($conn){
 echo "Connected to the <strong>$db</strong> database successfully!<br>";
 }
}catch (PDOException $e){
 // report error message
 echo $e->getMessage();
}
//prepare point SQL
$lon=$_POST['longitude'];
$lat=$_POST['latitude'];
//$geom="ST_GeomFromText('POINT(".$lon." ".$lat.")', 4326)"; //construct function string use either
$geom="ST_SetSRID(ST_MakePoint(".$lon.",".$lat."), 4326)"; //construct function string

//Check for duplicate National catalog number
$natcatnumber=$_POST['natcatnumb'];
if ($natcatnumber!='0000'){ //check only if not default value
echo 'Checking for duplicate National catalog number:'.$natcatnumber.'<br>';
$sql = "SELECT natcatnumb FROM public.objects WHERE natcatnumb='$natcatnumber';";
$stmt = $conn->prepare($sql);
$result=$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (!$result) {
 echo '<br>National catalog number is OK. No duplicates found<br>';
}
 else {
   echo "<br><b>Error! </b>National catalog number<b> $result[natcatnumb] </b> is duplicated<br>Go back and correct the number";die;
 }
} //end if

//check for duplicate Club catalog number
$clubcatnumber=$_POST['clubcatnumb'];
if ($clubcatnumber!='0000'){ //check only if not default value
echo 'Checking for duplicate Club catalog number:'.$clubcatnumber.'<br>';
$sql = "SELECT clubcatnumb FROM public.objects WHERE clubcatnumb='$clubcatnumber';";
$stmt = $conn->prepare($sql);
$result=$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (!$result) {
 echo '<br>Club catalog number is OK. No duplicates found<br>';
}
 else {
  echo "<br><b>Error! </b>Club catalog number <b> $result[clubcatnumb] </b> is duplicated<br>Go back and correct the number";die;
 }
} // end if

## Check for position difference
## If there exists any object closer then AllowedPositionDiff form new object
## invoke error and die Is using AllowedPositionDiff variable from config
## we use distance sphere functions that return values in meters
$sql01="SELECT ST_Distance_Sphere(geom, ST_GeomFromText('POINT($lon $lat)', 4326)) as distance, id , name
FROM public.objects
ORDER BY distance LIMIT 1;";
echo "SQL=$sql01<br>";
$stmt = $conn->prepare($sql01);
$result=$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
$distance=$result['distance']; //get distance to nearest object
$nearestname=$result['name']; //and its name
echo "Distance to nearest object $nearestname is $distance m";
if ($distance < $AllowedPositionDiff){ //if distance is less then Allowed Position Difference we have problem
 echo "<br><b>this object is too close to existing object $nearestname. This is probably location error or duplicate object. Check your data!!!<br>";
 die; //we will not go on 
 }
else { //distances are OK
 echo ' ... distance check is OK.<br>';
}


// try to find elevation if we have elevation from map = true in config file
if ($auto_elevation) {
  echo 'Auto elevation function enabled, finding elevation from map<br>';
  }
else {
  echo 'Auto elevation function disabled. Using entered value for elevation<br>';
} //end elevatiobn drill

// import geographic unit 1
echo 'Defining geographic units ...<br>';
$geogunit1="Not defined"; //this is in case point is out of ciurrent geometries and we dont find anything
$geogunit2="Not defined"; // in that case we default to Not defined category in wiki
$geogunit3="Not defined";

// geography unit divison unit 1 (natural geography)
$sql="SELECT t_geogunit1.name FROM t_geogunit1 WHERE ST_Contains( t_geogunit1.geom 
, ST_GeometryFromText('POINT($lon $lat)', 4326));";
$stmt = $conn->prepare($sql);
$result=$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (!$result) {
 echo 'No geography unit 1 found, defaulting to Category:Not defined<br>';
}
else {
 echo "<br>Geography unit 1 is: $result[name]";
 $geogunit1=$result[name];
}

// geography unit divison unit 2 (administrative division)
$sql="SELECT t_geogunit2.name FROM t_geogunit2 WHERE ST_Contains( t_geogunit2.geom 
, ST_GeometryFromText('POINT($lon $lat)', 4326));";
$stmt = $conn->prepare($sql);
$result=$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (!$result) {
 echo 'No geography unit 2 found, defaulting to Category:Not defined<br>';
}
else {
 echo "<br>Geography unit 2 is: $result[name]";
 $geogunit2=$result[name];
}

// geography unit divison unit 3 (cadastre units)
$sql="SELECT t_geogunit3.name FROM t_geogunit3 WHERE ST_Contains( t_geogunit3.geom 
, ST_GeometryFromText('POINT($lon $lat)', 4326));";
$stmt = $conn->prepare($sql);
$result=$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (!$result) {
 echo 'No geography unit 3 found, defaulting to Category:Not defined<br>';
}
else {
 echo "<br>Geography unit 3 is: $result[name]";
 $geogunit3=$result[name];
}




// prepare statement for insert

$sql = "INSERT INTO public.objects(pagename, 
geom, 
natcatnumb, 
clubcatnumb, 
code, 
name, 
nattype1, 
nattype2, 
mapsymbol, 
elevation, 
locatedby, 
accuracy, 
geogunit1, 
geogunit2, 
geogunit3, 
length, 
depth, 
measured, 
discoverydate, 
organization, 
author, 
entrycomment, 
comment, 
taccess, 
lasteditor, 
natregstatus, 
image, 
locationcomment, 
surface, 
volume, 
sync_level, 
sync_mask, 
sync_status, 
isvalid, 
research_level, 
entry_x, 
entry_y, 
condition_lnk) 

VALUES(
:pagename, 
$geom, 
:natcatnumb, 
:clubcatnumb, 
:code, 
:name, 
:nattype1, 
:nattype2, 
:mapsymbol, 
:elevation, 
:locatedby, 
:accuracy, 
:geogunit1, 
:geogunit2, 
:geogunit3, 
:length, 
:depth, 
:measured, 
:discoverydate, 
:organization, 
:author, 
:entrycomment, 
:comment, 
:taccess, 
:lasteditor, 
:natregstatus, 
:image, 
:locationcomment, 
:surface, 
:volume, 
:sync_level, 
:sync_mask, 
:sync_status, 
:isvalid, 
:research_level, 
:entry_x, 
:entry_y, 
:condition_lnk)";

//prepare variables
$pagename=$_POST['name'].' '.$_POST['natcatnumb'].' '.$_POST['code'];

$organization=rtrim($_POST['organization'],', ');  //trim off comma at the end
$author=rtrim($_POST['authors'],', '); //trim off comma at the end
$image="No_image.png"; //set deafult blank image
$research_level="1000"; //default level for new known caves
$isvalid=true; //it is new record so it MUST be valid
$sync_status="XYZ";   //fix status to default sync_status value 'char_variable'


$stmt = $conn->prepare($sql);

// pass values to the statement array
$stmt->bindValue(':pagename', $pagename);                     //constructed above
//$stmt->bindValue(':geom', $geom);                      //constructed above
$stmt->bindValue(':natcatnumb', $_POST['natcatnumb']); 
$stmt->bindValue(':clubcatnumb', $_POST['clubcatnumb']); 
$stmt->bindValue(':code', $_POST['code']); 
$stmt->bindValue(':name', $_POST['name']); 
$stmt->bindValue(':nattype1', $_POST['nattype1']); 
$stmt->bindValue(':nattype2', $_POST['nattype2']); 
$stmt->bindValue(':mapsymbol', $_POST['mapsymbol']); 
$stmt->bindValue(':elevation', $_POST['elevation']); 
$stmt->bindValue(':locatedby', $_POST['locatedby']); 
$stmt->bindValue(':accuracy', $_POST['accuracy']); 
$stmt->bindValue(':geogunit1', $geogunit1);                     //constructed above
$stmt->bindValue(':geogunit2', $geogunit2);                     //constructed above
$stmt->bindValue(':geogunit3', $geogunit3);                     //constructed above
$stmt->bindValue(':length', $_POST['length']); 
$stmt->bindValue(':depth', $_POST['depth']); 
$stmt->bindValue(':measured', $_POST['measured']);
$stmt->bindValue(':discoverydate', $_POST['date']);
$stmt->bindValue(':organization', $organization);                     //constructed above
$stmt->bindValue(':author', $author);                     //constructed above
$stmt->bindValue(':entrycomment', $_POST['entrycomment']);
$stmt->bindValue(':comment', $_POST['comment']);
$stmt->bindValue(':taccess', $_POST['taccess']);
$stmt->bindValue(':lasteditor', $_POST['lasteditor']);
$stmt->bindValue(':natregstatus', $_POST['natregstatus']);
$stmt->bindValue(':image', $image);                     //constructed above
$stmt->bindValue(':locationcomment', $_POST['locationcomment']);
$stmt->bindValue(':surface', $_POST['surface']);
$stmt->bindValue(':volume', $_POST['volume']);
$stmt->bindValue(':sync_level', $_POST['sync_level']);
$stmt->bindValue(':sync_mask', $_POST['sync_mask']);
$stmt->bindValue(':sync_status', $sync_status);                     //constructed above
$stmt->bindValue(':isvalid', $isvalid);                     //constructed above
$stmt->bindValue(':research_level', $research_level);                     //constructed above
$stmt->bindValue(':entry_x', $_POST['entrance_x']);
$stmt->bindValue(':entry_y', $_POST['entrance_y']);
$stmt->bindValue(':condition_lnk', $_POST['condition']);
echo '<br> saving record ...<br>';
// execute the insert statement
$result=$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
var_dump($result);

// proceed to page creation
// /var/www/html/maintenance# php edit.php -u "username" -s "Quick edit" -m Page_I_want_to_create < objectpage.txt
$pagename=str_replace(" ","_",$pagename); //replace spaces with _
$username=$_POST['lasteditor'];
$cmd="php /var/www/html/maintenance/edit.php -u '$username' -s 'Quick edit' -m $pagename < /var/www/html/maintenance/objectpage.txt";
echo "<br>Creating page for $pagename ...";
$r=shell_exec ( $cmd );
sleep(2);
echo "<br>$cmd";
echo "<br>$r";

// if this si new object that has research status 1000 go
// and create action page for this object
// with content
//
// {{Action
// |Intention=Surveying
// |Date=$_POST['date'])
// |Objects=$pagename
// |Weather=undefined
// |Description=Autogenerated action entry
// |Clubs=$organization
// |Participants=$author
// |Success=5}}
//
// This will call Action template and fill in fields and properties etc.

//  !!! But first we have to check if such action exists
// if it does we DO NOT create another one
if ($research_level>'999'){
 $mydate=$_POST['date'];
 $mycode=$_POST['code'];
 $act_pagename="Action_".$mydate."_Surveying_".$mycode; //we generate page name
 $cmd1="printf '{{Action
 |Intention=Surveying
 |Date=$mydate
 |Objects=$pagename
 |Weather=undefined
 |Description=Autogenerated action entry
 |Clubs=$organization
 |Participants=$author
 |Success=5}}'";
 $cmd2=" | php /var/www/html/maintenance/edit.php -u '$username' -s 'Quick edit' -m $act_pagename";
 $cmd=$cmd1.$cmd2;
 $r=shell_exec ( $cmd );
 sleep(2);
} //end if reaserach level
// --------------------- Finaly we go to the page generated
echo "<script>window.location = '$wikihost/index.php/$pagename'</script>";
die();

?>
