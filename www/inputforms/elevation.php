<?php
# short script utilizing calling
# KATJA elevation service
# this service is public and used by other katja
# instalations
#
# It expects lat and lon WGS84 parameters in url
$lat=$_GET['lat'];
$lon=$_GET['lon'];
$url="http://www.katjanet.net/DEM_services/elevation.php?lat=$lat&lon=$lon";
$result = file_get_contents($url);
$result = getBetween($result,"Value>","</Value"); #get actual value form string returned
echo $result;

function getBetween($string, $start = "", $end = ""){
    if (strpos($string, $start)) { // required if $start not exist in $string
        $startCharCount = strpos($string, $start) + strlen($start);
        $firstSubStr = substr($string, $startCharCount, strlen($string));
        $endCharCount = strpos($firstSubStr, $end);
        if ($endCharCount == 0) {
            $endCharCount = strlen($firstSubStr);
        }
        return substr($firstSubStr, 0, $endCharCount);
    } else {
        return '';
    }
}
?>
