<?php
# exposes some configuration
# parameters form KATJA_config.php
# as json for
# javascripts that ned that data
require_once ('../KATJA_config.php'); //read config file
$parameter =array(
"init_longitude"=>$init_longitude,
"init_latitude"=>$init_latitude,
"init_zoom"=>$init_zoom
);
echo json_encode($parameter);
?>
