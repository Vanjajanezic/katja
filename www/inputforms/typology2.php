<?php
/**
 *  Configuration
 * -------------------------------------------------
 */
// Start session
require_once '../KATJA_config.php'; //read config file
session_start();

// Login
global $CategoryName, $app, $postdata, $ch ;
$app['username'] = "Admin";
$app['password'] = "geslojeskrito";
$CategoryName = "Category:$NationalTypology1Name"; # --------------- Change this to read another typology. Put here root category


// Version
$app["version"] = "0.3.0";
// Last modified
date_default_timezone_set("UTC");
$app["lastmod"] = date("Y-m-d H:i", getlastmod()) . " UTC"; // Example: 2010-04-15 18:09 UTC
// User-Agent used for loading external resources
$app["useragent"] = "KATJA GetCatTree " . $app["version"] . " (LastModified: " . $app["lastmod"] . ") Contact: myfirsttool (at) example (.) com";
// Cookie file for the session
$app["cookiefile"] = tempnam("/tmp", "CURLCOOKIE");

$app["port"]=80;
// cURL to avoid repeating ourselfs
$app["curloptions"] =
	array(
		CURLOPT_COOKIEFILE => $app["cookiefile"],
		CURLOPT_COOKIEJAR => $app["cookiefile"],
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_USERAGENT => $app["useragent"],
        CURLOPT_PORT=>$app["port"],
		CURLOPT_POST => true
	);
$app["apiURL"] = "$wikihost/api.php";


## *  Login
## * -------------------------------------------------
$postdata = http_build_query([
    "action" => "login",
    "format" => "php",
    "lgname" => $app["username"],
    "lgpassword" => $app["password"],
]);

$ch = curl_init();
	curl_setopt_array($ch, $app["curloptions"]);
	curl_setopt($ch, CURLOPT_URL, $app["apiURL"]);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
	$result = unserialize(curl_exec($ch));
	if(curl_errno($ch)){
    	$curl_error =  "Error 003: " . curl_error($ch);
	}
curl_close($ch);
//print_r($result);//die;//DEBUG

// Basic error check + Confirm token
if ($curl_error){
	$domain_error = $curl_error;

} else if ($result["login"]["result"] == "NeedToken") {

	if (!empty($result["login"]["token"])) {
		$_SESSION["logintoken"] = $result["login"]["token"];

		$postdata = http_build_query([
		    "action" => "login",
		    "format" => "php",
		    "lgname" => $app["username"],
		    "lgpassword" => $app["password"],
		    "lgtoken" => $_SESSION["logintoken"],
		]);

		$ch = curl_init();
			curl_setopt_array($ch, $app["curloptions"]);
			curl_setopt($ch, CURLOPT_URL, $app["apiURL"]);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			$result = unserialize(curl_exec($ch));
			if(curl_errno($ch)){
		    	$curl_error =  "Error 004: " . curl_error($ch);
			}
		curl_close($ch);
		//print_r($result);//die;//DEBUG

	} else {
		$other_error = "Error 006: Token error.";
	}

}


// *********************************************************************************   Let us do something here
$idx=0; //start index counter
$Tarray = array(); //define array
$result=GetCategoryList($CategoryName); #---------------------------------0 level
$rarray=json_decode($result,true);
$kategorije=$rarray['query']['categorymembers'];
foreach ($kategorije as $key=>$vrednost) {
    if ($vrednost['ns']=='14') {
        // strip off Category: or whatever is before : and leave just name of category
        $Tarray[$idx]['val']=$vrednost['title'];$Tarray[$idx]['label']=substr($vrednost['title'],strpos($vrednost['title'],':')+1);$idx++;
        $CategoryName=$vrednost['title'];
        $result_1=GetCategoryList($CategoryName); #--------------------- 1 level
        $rarray_1=json_decode($result_1,true);
        $kategorije_1=$rarray_1['query']['categorymembers'];
        foreach ($kategorije_1 as $key=>$vrednost_1) {
            if ($vrednost_1['ns']=='14') {
                $Tarray[$idx]['val']=$vrednost_1['title'];$Tarray[$idx]['label']='...'.substr($vrednost_1['title'],strpos($vrednost_1['title'],':')+1);$idx++;
                $CategoryName=$vrednost_1['title'];
                $result_2=GetCategoryList($CategoryName); # ------------ 2 level
                $rarray_2=json_decode($result_2,true);
                $kategorije_2=$rarray_2['query']['categorymembers'];
                foreach ($kategorije_2 as $key=>$vrednost_2) {
                    if ($vrednost_2['ns']=='14') {
                        $Tarray[$idx]['val']=$vrednost_2['title'];$Tarray[$idx]['label']='......'.substr($vrednost_2['title'],strpos($vrednost_2['title'],':')+1);$idx++;
                }
                }
            } //end if   
        }
    } //end if
}

// **************************************************************************end of doing something sensible

echo json_encode($Tarray); //give back json array


// --------------------------------------------------------------------------------------------------------THE END OF STORY
// Delete the cookie file
unlink($app["cookiefile"]);
// Destroy the session
session_destroy();
// End this file
die($output);

// --------------------------------------------------------------------------------------------------------functions section
function GetCategoryList($CategoryName){
## *************** Function GetCategoryList ******************
# get json necoded list of category members from MediaWiki API
# category name is passed as parameter
global $CategoryName, $app, $postdata, $ch ;
		$postdata = http_build_query([
		    "action" => "query",
		    "format" => "json",
		    "list" => "categorymembers",
		    "cmtitle" => $CategoryName,
		    "cmlimit" =>"2000",
		]);
		$ch = curl_init();
			curl_setopt_array($ch, $app["curloptions"]);
			curl_setopt($ch, CURLOPT_URL, $app["apiURL"]);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			$result = curl_exec($ch);
		curl_close($ch);
return $result;
} #End of function
## ****************************************





?>

