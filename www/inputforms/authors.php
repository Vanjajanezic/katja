<?php
## KATJA script
## PHP script returns clean up array of authors from database
## to be used with JQuery in autocomplete fields of forms
## somehow awkward way of doing it, but we need to
## split row where two or more authors are listed
## and have to eliminate duplicate entries
$db = pg_connect("host=katjadb port=5432 dbname=katja user=wikiuser password=katjasecret");
$result = pg_query($db,"SELECT author FROM objects"); # can change this to suit your needs
# from here we read database and add results to variable
# longstring,,, which we will use at the end to make array out of it
$row=pg_fetch_assoc($result); # we do single read to ..
$longstring=$row['author'];  # avoid having extra comma delimiter in string 
while($row=pg_fetch_assoc($result)){ # and we read the rest of the bunch
$longstring=$row['author'].','.$longstring;
}
$longstring=str_replace(", ",",",$longstring); // replace space comma with comma
$tmparr=explode(",",$longstring); //converting string to array
$tmparr2 = array_unique($tmparr); //removing duplicates
# and finally output array as json encoded
echo json_encode($tmparr2);
?>
