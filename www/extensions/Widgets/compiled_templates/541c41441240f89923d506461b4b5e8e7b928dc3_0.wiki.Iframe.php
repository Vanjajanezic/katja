<?php
/* Smarty version 3.1.31, created on 2018-03-14 11:52:12
  from "wiki:Iframe" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5aa8fedc449d24_98398102',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '541c41441240f89923d506461b4b5e8e7b928dc3' => 
    array (
      0 => 'wiki:Iframe',
      1 => 20180220094756,
      2 => 'wiki',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5aa8fedc449d24_98398102 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_validate')) require_once '/var/www/katja/wiki/extensions/Widgets/smarty_plugins/modifier.validate.php';
?>
<iframe src="<?php echo smarty_modifier_validate($_smarty_tpl->tpl_vars['url']->value,'url');?>
" style="border: <?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['border']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? 0 : $tmp);?>
" width="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['width']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? 400 : $tmp);?>
" height="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['height']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? 300 : $tmp);?>
"></iframe><?php }
}
