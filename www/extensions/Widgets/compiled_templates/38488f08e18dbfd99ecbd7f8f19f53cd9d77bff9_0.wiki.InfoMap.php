<?php
/* Smarty version 3.1.31, created on 2018-06-22 11:27:04
  from "wiki:InfoMap" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b2cdd08a7b101_06380485',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '38488f08e18dbfd99ecbd7f8f19f53cd9d77bff9' => 
    array (
      0 => 'wiki:InfoMap',
      1 => 20180620084035,
      2 => 'wiki',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b2cdd08a7b101_06380485 (Smarty_Internal_Template $_smarty_tpl) {
?>


 <?php echo '<script'; ?>
 src="http://www.katjanet.net/mapsaux/js/leaflet.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 type="text/javascript">
var lon= '<?php echo $_smarty_tpl->tpl_vars['longitude']->value;?>
';
var lat = '<?php echo $_smarty_tpl->tpl_vars['latitude']->value;?>
';
var zoomlevel='<?php echo $_smarty_tpl->tpl_vars['zoomlevel']->value;?>
';

        var map = L.map("map").setView([lat, lon], zoomlevel);
        
        L.tileLayer(
            "http://a.tile.stamen.com/terrain/{z}/{x}/{y}.png", {
                 maxZoom: 18,
            }).addTo(map);
       L.control.scale({metric: true, imperial: false}).addTo(map);
var marker = L.marker([lat, lon],{
  draggable: false
}).addTo(map);
    <?php echo '</script'; ?>
>
<?php }
}
