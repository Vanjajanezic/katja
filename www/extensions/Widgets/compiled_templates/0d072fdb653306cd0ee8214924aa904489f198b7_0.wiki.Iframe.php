<?php
/* Smarty version 3.1.31, created on 2018-05-04 18:04:03
  from "wiki:Iframe" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5aec8473c15451_75825871',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d072fdb653306cd0ee8214924aa904489f198b7' => 
    array (
      0 => 'wiki:Iframe',
      1 => 20180316071523,
      2 => 'wiki',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5aec8473c15451_75825871 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_validate')) require_once '/var/www/katja/wiki/extensions/Widgets/smarty_plugins/modifier.validate.php';
?>
<iframe src="<?php echo smarty_modifier_validate($_smarty_tpl->tpl_vars['url']->value,'url');?>
" style="border: <?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['border']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? 0 : $tmp);?>
" width="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['width']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? 400 : $tmp);?>
" height="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['height']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? 300 : $tmp);?>
"></iframe><?php }
}
