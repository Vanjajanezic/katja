<?php
/**
 * Internationalization file for the magic word of the PhpTags extension.
 *
 * @file PhpTags.i18n.magic.php
 * @ingroup PhpTags
 * @author Pavel Astakhov <pastakhov@yandex.ru>
 */

$magicWords = array();

/** English
 * @author pastakhov
 */
$magicWords['en'] = array(
	'phptag' => array( 0, 'phptag' ),
);
