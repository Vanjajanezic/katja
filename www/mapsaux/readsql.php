<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
         <meta http-equiv="Cache-control" content="public">
        <link rel="stylesheet" href="css/leaflet.css">
        <link rel="stylesheet" href="css/qgis2web.css">
        <style>
        #map {
            width: 500px;
            height: 300px;
        }
        </style>
        <title>SQL karta</title>
    </head>
    <body>
        <div id="map">
        </div>
        <script src="js/qgis2web_expressions.js"></script>
        <script src="js/leaflet.js"></script>
        <script src="js/leaflet.rotatedMarker.js"></script>
        <script src="js/leaflet.pattern.js"></script>
        
        <script src="js/Autolinker.min.js"></script>
        <script src="js/rbush.min.js"></script>
        <script src="js/labelgun.min.js"></script>
        <script src="js/labels.js"></script>
        <script src="js/leaflet.wms.js"></script>

<?php
// Connecting, selecting database
$dbconn = pg_connect("host=localhost dbname=katja user=postgres password=geslojeskrito")
    or die('Could not connect: ' . pg_last_error());

//getting passed name
$fullname = $_GET['object'];
echo 'Passed name is:'.$fullname.'<br>';

$fullname="'".$fullname."'";
// Performing SQL query
// change  called view to change language (descrpage_english)
$query = 'SELECT * FROM descrpage_slovene WHERE name='.$fullname;

$result = pg_query($query) or die('Query failed: ' . pg_last_error());
echo $result;

echo '<br><hr><br>';
$arr = pg_fetch_array($result, 0, PGSQL_NUM); //move result to array


echo "\n<form><label for='latitude'>Latitude:</label><input id='latitude' name='latitude' type='text' value='".$arr[9]."' /><br>
      <label for='longitude'>Longitude:</label><input id='longitude' name='longitude' type='text' value='".$arr[8]."'/><br>
	<label for='hash'>hash:</label><input id='hash' type='text'/><br>
    <label for='zoom'>Zoom:</label><input id='zoom' type='text'/><br>
	<label for='lat'>Latitude:</label><input id='lat' type='text'/><br>
	<label for='lon'>Longitude:</label><input id='lon' type='text'/><br>
	<label for='name'>Name:</label><input id='name' type='text'/><br>
</form>";

echo "Pagename: ".$arr[0] . "<br>";
echo "image file: ".$arr[1] . "<br>";
echo "national catalog number: ".$arr[2] . "<br>";
echo "club catalog nummber: ".$arr[3] . "<br>";
echo "code: ".$arr[4] . "<br>";
echo "Name: ".$arr[5] . "<br>";
echo "national type 1: ".$arr[6] . "<br>";
echo "national type 2: ".$arr[7] . "<br>";
echo "longitude x: ".$arr[8] . "<br>";
echo "latitude Y: ".$arr[9] . "<br>";
//echo "elevation: ".$arr[10] . "<br>";
//echo "located by: ".$arr[11] . "<br>";
//echo "access: ".$arr[12] . "<br>";
//echo "nat reg status: ".$arr[13] . "<br>";
//echo "map symbol: ".$arr[14] . "<br>";
//echo "accuracy: ".$arr[15] . "<br>";
//echo "geogunit1: ".$arr[16] . "<br>";
//echo "geogunit2: ".$arr[17] . "<br>";
//echo "geogunit3: ".$arr[18] . "<br>";
//echo "length: ".$arr[19] . "<br>";
//echo "depth: ".$arr[20] . "<br>";
//echo "measured: ".$arr[21] . "<br>";
//echo "disovery date: ".$arr[22] . "<br>";
//echo "author: ".$arr[23] . "<br>";
//echo "organization: ".$arr[24] . "<br>";
//echo "comment: ".$arr[25] . "<br>";
//echo "condition: ".$arr[26] . "<br>";
//echo "entry comment: ".$arr[27] . "<br>";
//echo "last editor: ".$arr[28] . "<br>";
//echo "timestamp: ".$arr[29] . "<br>";
//echo "entry history: ".$arr[30] . "<br>";

// Free resultset
pg_free_result($result);

// Closing connection
pg_close($dbconn);

echo "\n\n<script>
// ***************************************** START of SCRIPT *********************************

//preberemo vrednosti iz formularja       

lat=document.getElementById('latitude').value;
lon=document.getElementById('longitude').value;
zoom=12;
document.write('To so vrednosti ');document.write(lat);document.write(' - ');document.write(lon);


        var map = L.map('map', {
		'center': [lat, lon],
		'zoom': zoom,
		zoomControl:true,
		maxZoom:18,
		minZoom:10
        }).fitBounds([[45.2946082499,12.7500089662],[46.7919650156,16.7189798408]]);


        function setBounds() {
        }

      
        var dmr = L.WMS.layer('http://katja.jknm.si:8080/geoserver/wms?version=1.1.1&', 'katja:shades2', {
            format: 'image/png',
            uppercase: true,
            transparent: true,
            continuousWorld : true,
            tiled: true,
            info_format: 'text/html',
            opacity: 1,
            identify: false,
            zIndex: 1
        });
       map.addLayer(dmr);

        var karta = L.WMS.layer('http://katja.jknm.si:8080/geoserver/wms?version=1.1.1&', 'katja:karta50', {
            format: 'image/png',
            uppercase: true,
            transparent: true,
            continuousWorld : true,
            tiled: true,
            info_format: 'text/html',
            maxZoom: 18,
            minZoom: 15,
            opacity: 1,
            identify: false,
            zIndex: 2
        });
        map.addLayer(karta);

        var jame = L.WMS.layer('http://katja.jknm.si:8080/geoserver/wms?version=1.1.1&', 'katja:objekti2', {
            format: 'image/png8',
            uppercase: true,
            transparent: true,
            continuousWorld : true,
            tiled: true,
            info_format: 'text/html',
            opacity: 1,
            zIndex: 3
        });
        map.addLayer(jame);


//Tole je novo
var marker = L.marker([lat, lon],{
  draggable: true
}).addTo(map);
// ******* TULE SPREMINJAMO VREDNOSTI v formularju ***************+**
marker.on('dragend', function (e) {
  document.getElementById('latitude').value = marker.getLatLng().lat;
  document.getElementById('longitude').value = marker.getLatLng().lng;
});
//konec vstavljenega

L.control.scale({metric: true, imperial: false}).addTo(map);
var baseMaps = {'DMR': dmr};

var overlayMaps = {
    'Objekti': jame,
    '1:50.000': karta,
    
};
L.control.layers(baseMaps, overlayMaps).addTo(map);

map.on('overlayadd', function(layer) {
jame.bringToFront();
});
        setBounds();

   </script>
</body>
</html>"
?>





