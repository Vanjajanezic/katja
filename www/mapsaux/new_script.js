
var tileLayer = new L.TileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',{
  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
});
        var dmr = L.WMS.layer("http://193.189.165.224:8080/geoserver/wms?version=1.1.1&", "karte:shades", {
            format: 'image/png',
            uppercase: true,
            transparent: true,
            continuousWorld : true,
            tiled: true,
            info_format: 'text/html',
            opacity: 1,
            identify: false,
            zIndex: 1
        });
       map.addLayer(dmr);

var map = new L.Map('map', {
  'center': [46.0453, 14.5119],
  'zoom': 12,
  'layers': [tileLayer,dmr]
});

var marker = L.marker([46.0453, 14.5119],{
  draggable: true
}).addTo(map);

marker.on('dragend', function (e) {
  document.getElementById('latitude').value = marker.getLatLng().lat;
  document.getElementById('longitude').value = marker.getLatLng().lng;
});
