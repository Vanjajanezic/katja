CREATE VIEW public.descrpage_english AS
 SELECT objects.id,
    objects.pagename,
    objects.image,
    objects.natcatnumb,
    objects.clubcatnumb,
    objects.code,
    objects.name,
    objects.nattype1,
    objects.nattype2,
    (public.st_x(objects.geom))::text AS x,
    (public.st_y(objects.geom))::text AS y,
    objects.elevation,
    mapsymbols.english AS mapsymbol,
    locatedby.english AS vlocatedby,
    taccess.english AS vaccess,
    natregstatus.english AS vnatregstatus,
    conditions.english AS condition,
    objects.accuracy,
    objects.geogunit1,
    objects.geogunit2,
    objects.geogunit3,
    objects.length,
    objects.depth,
    objects.measured,
    objects.discoverydate,
    objects.author,
    objects.organization,
    objects.comment,
    objects.entrycomment,
    objects.lasteditor,
    objects.created_at,
    objects.locationcomment,
    objects.surface,
    objects.volume,
    objects.sync_level,
    objects.sync_mask,
    objects.sync_status,
    objects.research_level,
    objects.entry_x,
    objects.entry_y
   FROM (((((public.objects
     LEFT JOIN public.locatedby ON ((objects.locatedby = locatedby.id)))
     LEFT JOIN public.taccess ON ((objects.taccess = taccess.id)))
     LEFT JOIN public.natregstatus ON ((objects.natregstatus = natregstatus.id)))
     LEFT JOIN public.conditions ON ((objects.condition_lnk = conditions.id)))
     LEFT JOIN public.mapsymbols ON ((objects.mapsymbol= mapsymbols.id)))
 WHERE isvalid;

ALTER TABLE public.descrpage_english OWNER TO postgres;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO wikiuser;
