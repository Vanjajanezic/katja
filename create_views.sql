-- View: public.descrpage_english

-- DROP VIEW public.descrpage_english;

CREATE OR REPLACE VIEW public.descrpage_english AS
 SELECT objects.pagename,
    objects.image,
    objects.natcatnumb,
    objects.clubcatnumb,
    objects.code,
    objects.name,
    objects.nattype1,
    objects.nattype2,
    st_x(objects.geom)::text AS x,
    st_y(objects.geom)::text AS y,
    objects.elevation,
    locatedby.english AS vlocatedby,
    taccess.english AS vaccess,
    natregstatus.english AS vnatregstatus,
    objects.mapsymbol,
    objects.accuracy,
    objects.geogunit1,
    objects.geogunit2,
    objects.geogunit3,
    objects.length,
    objects.depth,
    objects.measured,
    objects.discoverydate,
    objects.author,
    objects.organization,
    objects.comment,
    objects.condition,
    objects.entrycomment,
    objects.lasteditor,
    objects."timestamp",
    objects.entryhistory,
    objects.locationcomment,
    objects.surface,
    objects.volume,
    objects.sync_level
   FROM objects
     LEFT JOIN locatedby ON objects.locatedby = locatedby.id
     LEFT JOIN taccess ON objects.taccess = taccess.id
     LEFT JOIN natregstatus ON objects.natregstatus = natregstatus.id;

ALTER TABLE public.descrpage_english
    OWNER TO postgres;
COMMENT ON VIEW public.descrpage_english
    IS 'Description page data view in english value mapings';
-- Slovene view
-- View: public.descrpage_slovene

-- DROP VIEW public.descrpage_slovene;

CREATE OR REPLACE VIEW public.descrpage_slovene AS
 SELECT objects.pagename,
    objects.image,
    objects.natcatnumb,
    objects.clubcatnumb,
    objects.code,
    objects.name,
    objects.nattype1,
    objects.nattype2,
    st_x(objects.geom)::text AS x,
    st_y(objects.geom)::text AS y,
    objects.elevation,
    locatedby.slovene AS vlocatedby,
    taccess.slovene AS vaccess,
    natregstatus.slovene AS vnatregstatus,
    objects.mapsymbol,
    objects.accuracy,
    objects.geogunit1,
    objects.geogunit2,
    objects.geogunit3,
    objects.length,
    objects.depth,
    objects.measured,
    objects.discoverydate,
    objects.author,
    objects.organization,
    objects.comment,
    objects.condition,
    objects.entrycomment,
    objects.lasteditor,
    objects."timestamp",
    objects.entryhistory,
    objects.locationcomment,
    objects.surface,
    objects.volume,
    objects.sync_level
   FROM objects
     LEFT JOIN locatedby ON objects.locatedby = locatedby.id
     LEFT JOIN taccess ON objects.taccess = taccess.id
     LEFT JOIN natregstatus ON objects.natregstatus = natregstatus.id;

ALTER TABLE public.descrpage_slovene
    OWNER TO postgres;
COMMENT ON VIEW public.descrpage_slovene
    IS 'Podatki za opisno stran v slovenskih vrednostih';

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO wikiuser;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO wikiuser;

